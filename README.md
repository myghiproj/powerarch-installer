# MygOS Installer

This project will be a complete rewrite from [AAIS](https://gitlab.com/myghiproj/aais).

## Features

- [X] Full hardware acceleration support for Intel and AMD (NVidia users: please install the driver separately);
- [X] Very optimized zram-only swapping by default, tested on a lot of hardware sets to ensure the best experience;
- [X] Linux-zen kernel, which already contains the neccessary modules for waydroid and have a bunch of performance improvements for everyday usage and gaming;
- [X] Unicode, CJK and emoji characters with noto-fonts family;
- [X] KDE Plasma Wayland desktop environment, which offers a lot of features including freesync, fractional scaling, required protocols for Virtual Reality and more;
- [X] Full disk encryption (with unencrypted /boot);
- [X] Multiboot support (automatically fills empty space on target disk).

## Minimum Hardware/VM Requirements

- [X] Any 64-bit capable X86 CPU
- [X] 2GB of RAM (At least 4GB is recommended)
- [X] 8GB of Storage (Recommended at least 16GB to be usable in a long term)
- [X] Any compatible GPU (NVidia users may have a worse experience overall)

These hardware requirements may vary depending on your workload.

## License

- [X] GNU GPLv3 - Please read license.txt

## Progress

It's not usable yet. The main script is mostly done and some features are currently being decided.
