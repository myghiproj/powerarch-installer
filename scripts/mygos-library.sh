#!/usr/bin/env bash

# Functions that can be used on multiple scripts and/or would be better to be here
# Copyright (C) 2022 - Myghi63 (contato-myghi63@protonmail.com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


WARNING="$(tput bold smso setaf 1)"
TIPS="$(tput bold setaf 2)"
BOLD=$(tput bold)
NORMAL=$(tput sgr0)
REVERSE=$(tput smso)

NavigationTips() {
    printf "%s\n\n" "${TIPS}Dont forget: Press Q to exit the next list and use arrow keys to navigate.${NORMAL}"
    read -p "Press any key to continue: " -n 1 -r
}

InvalidGenericError() {
    printf "%s\n\n" "${WARNING}You have typed a invalid option! Please try again.${NORMAL}"
    return 1
}

InvalidStorageError() {
    printf "%s\n\n" "${WARNING}This device or partition does not exist!${NORMAL}"
    return 1
}

PasswordMismatchError() {
    printf "%s\n\n" "${WARNING}Password Mismatch! Please try again.${NORMAL}"
    return 1
}

IsDeviceOrPartition() {
    local block="$1"
    local shouldBePartition="$2"
    local isPartition

    if echo "$block" | grep -q "nvme" || echo "$1" | grep -q "mmcblk"
    then
        # Check for emmc and nvme devices
        echo "$block" | grep -q "p" && isPartition=true || isPartition=false
    else
        # Check for devices named as sda / vda / etc.
        echo "$block" | grep -q "[0-9]" && isPartition=true || isPartition=false
    fi

    if "$isPartition"
    then
        "$shouldBePartition" || {
            printf "%s\n\n" "${WARNING}Please select a storage drive, not a partition!${NORMAL}"
            return 1
        }
    else
        (! "$shouldBePartition") || {
            printf "%s\n\n" "${WARNING}Please select a partition, not a storage drive!${NORMAL}"
            return 1
        }
    fi
}

IsPartitionChild() {
    local partition="$1"
    local device="$2"

    echo "$partition" | grep -q "$device" || {
        printf "%s\n\n" "${WARNING}The partition should belong to the previously selected storage device!${NORMAL}"
        return 1
    }
}

IsPartitionEqual() {
    local partition1="$1"
    local partition2="$2"

    [ "$partition1" != "$partition2" ] || {
        printf "%s\n\n" "${WARNING}You already selected this partition for another thing!${NORMAL}"
        return 1
    }
}
