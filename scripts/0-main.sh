#!/usr/bin/env bash

# Gather the necessary information for installing the OS
# Copyright (C) 2022 - Myghi63 (contato-myghi63@protonmail.com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# status (boolean)
# preset (char)
# keyboard locale region desktop encryption storage rootPartition bootPartition efiPartition username password (string)

# load library
source mygos-library.sh

clear

Preset() {
    local preset

    printf "%s\n" "${REVERSE}Please choose a Preset:${NORMAL}" \
    "${BOLD}" \
    "   [0] US English, with us keyboard (Default) and mirrors from United States" \
    "   [1] Brazilian Portuguese, with br-abnt2 keyboard and mirrors from Brazil" \
    "   [2] I want to set this up manually" \
    "${NORMAL}"

    read -p "Your choice: " -r preset
    clear
    preset="${preset//\ /}"

    case "$preset" in
        "0" | "")
            keyboard="us"
            locale="en_US.UTF-8"
            mirrors="United States"
            ;;
        "1")
            keyboard="br-abnt2"
            locale="pt_BR.UTF-8"
            mirrors="Brazil"
            ;;
        "2")
            ;;
        *)
            InvalidGenericError
            ;;
    esac
}

MirrorListDownload() {
    printf "%s\n\n" "${TIPS}Downloading mirror list, please wait...${NORMAL}"
    mirrorList="$(curl 'https://archlinux.org/mirrorlist/?country=all&ip_version=4&use_mirror_status=on')"
    mirrorCountries="$(echo "$mirrorList" | sed -n '5,$s/^## \?\(.*\)$/\1/p' | sort -u))"
    clear
}

MirrorConfiguration() {
    printf "%s\n" "${REVERSE}Let's select the server for downloading packages!${NORMAL}" \
    "${BOLD}" \
    "   [0 or 'United States'] Find the fastest servers from United States" \
    "   [1 or Brazil] Find the fastest servers from Brazil (note: these mirrors are mostly trash)" \
    "   Or something else (type ? to show all options)" \
    "${NORMAL}"

    read -p "Your choice: " -r mirrors
    clear

    case "$mirrors" in
        "?")
            NavigationTips
            echo "$mirrorCountries" | less
            return 1
            ;;
        "0" | "")
            mirrors="United States"
            ;;
        "1")
            mirrors="Brazil"
            ;;
        *)
            echo "$mirrorCountries" | grep -xq "$mirrors" || {
                mirrors=""
                InvalidGenericError
            }
            ;;
    esac
}

MirrorTester() {
    local rankMirrors
    local confirmation
    local sync

    printf "%s\n\n" "${TIPS}Testing mirrors, please wait...${NORMAL}"

    rankMirrors=$(echo "$mirrorList" | awk '/^## '"$mirrors"'|## Worldwide/{flag=1;next} /^##/{flag=0} flag && /^#Server/{print substr($0, 2)}')

    echo "$rankMirrors" | rankmirrors -n 6 -m 2 - > /etc/pacman.d/mirrorlist

    # Avoid a Brazilian mirror that usually returns error 403 during installation
    sed -i '/br.mirror.archlinux-br/s/^/#/' /etc/pacman.d/mirrorlist

    sync=true
    pacman -Syy || sync=false

    if "$sync"
    then
        printf "\n%s\n\n" "${BOLD}Is it okay to use the selected mirrors?${NORMAL}"
        read -p "Your choice [Y/n]: " -r confirmation
        clear

        [[ "$confirmation" == "y" || "$confirmation" == "Y" ]] || {
            mirrors=""
            return 1
        }
    else
        clear
        mirrors=""
        printf "%s\n\n" "${WARNING}Please select a different country.${NORMAL}"
        return 1
    fi
}

Mirrors() {
    [ -n "$mirrorList" ] || MirrorListDownload

    [ -z "$mirrors" ] || {
        if MirrorConfiguration
        then
            MirrorTester
        else
            return 1
        fi
    }
}

Keyboard() {
    [ -n "$keyboard" ] || {
        printf "%s\n" "${REVERSE}Now configure your keyboard layout:${NORMAL}" \
        "${BOLD}" \
        "   [0 or us] US default keyboard (Default)" \
        "   [1 or br-abnt2] Brazilian abnt2 keyboard" \
        "   Or something else (type ? to show all options)" \
        "${NORMAL}"

        read -p "Your choice: " -r keyboard
        clear
        keyboard="${keyboard//\ /}"

        case "$keyboard" in
            "0" | "")
                keyboard="us"
                ;;
            "1" )
                keyboard="br-abnt2"
                ;;
            "?")
                NavigationTips
                find /usr/share/kbd/keymaps/ -name '*.map.gz' | sed -e 's/\.map\.gz$//' -e 's@.*/@@' | less
                clear
                return 1
                ;;
            *)
                [ "$(find /usr/share/kbd/keymaps/ -name "${keyboard}*" -print -quit)" != "" ] || {
                    keyboard=""
                    InvalidGenericError
                }
                ;;
        esac
    }
}

Locale() {
    [ -n "$locale" ] || {
        printf "%s\n" "${REVERSE}Let's set the correct locale for your system:${NORMAL}" \
        "${BOLD}" \
        "   [0 or en_US.UTF-8] US English (UTF-8) (Default)" \
        "   [1 or pt_BR.UTF-8] Brazilian Portuguese (UTF-8)" \
        "   Or something else (type ? to show all options)" \
        "${NORMAL}" \
        "Note: en_US.UTF-8 is always enabled to improve compatibility," \
        "but if you only need that, you have to select it." \
        ""

        read -p "Your choice: " -r locale
        clear
        locale="${locale//\ /}"

        case "$locale" in
            "0" | "")
                locale="en_US.UTF-8"
                ;;
            "1")
                locale="pt_BR.UTF-8"
                ;;
            "?")
                NavigationTips
                sed -e '1,23d;' -e 's/#//g' /etc/locale.gen | less
                clear
                return 1
                ;;
            *)
                grep -xq "#\?${locale}[[:space:]].*" /etc/locale.gen || {
                    locale=""
                    InvalidGenericError
                }
                ;;
        esac
    }
}

Region() {
    printf "%s\n" "${REVERSE}Please type which is the region you live on:${NORMAL}" \
    "${BOLD}" \
    "   [0 or America/New_York] USA, New York time (UTC-4) (Default)" \
    "   [1 or America/Sao_Paulo] Brazil, Sao Paulo time (UTC-3)" \
    "   Or something else (type ? to show all options)" \
    "${NORMAL}"

    read -p "Your choice: " -r region
    clear
    region="${region//\ /}"

    case "$region" in
        "0" | "")
            region="America/New_York"
            ;;
        "1")
            region="America/Sao_Paulo"
            ;;
        "?")
            NavigationTips
            timedatectl list-timezones
            clear
            return 1
            ;;
        *)
            timedatectl set-timezone "$region" &> /dev/null || {
                InvalidGenericError
            }
            ;;
    esac
}

Desktop() {
    printf "%s\n" "${REVERSE}Which desktop environment you want to use?${NORMAL}" \
    "${BOLD}" \
    "   [0] KDE Plasma (Default)" \
    "   [1] Gnome" \
    "   [2] CLI Mode (Bring your own DE/WM later or use as a server)" \
    "${NORMAL}"

    read -p "Your Choice: " -r desktop
    clear
    desktop="${desktop//\ /}"

    case "$desktop" in
        "0" | "")
            desktop="plasma"
            ;;
        "1")
            desktop="gnome"
            ;;
        "2")
            desktop="cli"
            ;;
        *)
            InvalidGenericError
            ;;
    esac
}

Encryption() {
    local password1
    local password2

    printf "%s\n" "${REVERSE}Do you want to encrypt the root partition?${NORMAL}" \
    "" \
    "This will prevent unauthorized access of all your data," \
    "but the password will be required on every startup." \
    "${BOLD}" \
    "To enable encryption, please type a password, but don't use" \
    "special characters that are not avaliable on US keyboard." \
    "For security reasons, everything you type below will be hidden!" \
    "" \
    "If you want to disable encryption, please type 0 (number zero)." \
    "${NORMAL}"

    read -p "Your choice (or password): " -r -s password1
    printf "\n"

    case "$password1" in
        "0" | "")
            encryption="0"
            clear
            ;;
        *)
            read -p "Please type the password again: " -r -s password2
            clear

            if [ "$password1" == "$password2" ]
            then
                encryption="$password1"
            else
                PasswordMismatchError
            fi
            ;;
    esac
}

Storage() {
    printf "%s\n" "${REVERSE}Please type which storage drive you want to install.${NORMAL}" \
    "" \
    "Later you will be able to setup multi-boot and encryption!" \
    "${BOLD}" \
    "Examples: /dev/sda, /dev/nvme0n1, /dev/mmcblk0 ..." \
    "" \
    "If you want to see which drives and partitions are avaliable," \
    "please type ? and press enter." \
    "${NORMAL}"

    read -p "Your Choice: " -r storage
    clear
    storage="${storage//\ /}"

    case "$storage" in
        "?")
            NavigationTips
            fdisk -l | less
            clear
            return 1
            ;;
        *)
            if [ -b "$storage" ]
            then
                IsDeviceOrPartition "$storage" "false"
            else
                InvalidStorageError
            fi
            ;;
    esac
}

MultiBootRoot() {
    printf "%s\n" "${REVERSE}Do you want to install Arch on specific partitions?${NORMAL}" \
    "" \
    "This is userful for installing the new OS alongside" \
    "another one, like Windows or another Linux distro." \
    "${BOLD}" \
    "In order to use it, please type which partition you want to" \
    "format as root! Like /dev/sda3, /dev/nvme0n1p4 ..." \
    "" \
    "If you want to see which partitions are avaliable," \
    "please type ? and press enter." \
    "" \
    "But if you just want to erase the selected storage and" \
    "install PowerArch, please type 0 (number zero)." \
    "${NORMAL}"

    read -p "Your choice (or partition): " -r rootPartition
    clear
    rootPartition="${rootPartition//\ /}"

    case "$rootPartition" in
        "")
            InvalidGenericError
            ;;
        "0")
            efiPartition="0"
            bootPartition="0"
            ;;
        "?")
            NavigationTips
            fdisk -l "$storage" | less
            clear
            return 1
            ;;
        *)
            if [ -b "$rootPartition" ]
            then
                IsDeviceOrPartition "$rootPartition" "true"
                IsPartitionChild "$rootPartition" "$storage"
            else
                InvalidStorageError
            fi
            ;;
    esac
}

MultiBootBoot() {
    [[ "$encryption" == "0" || "$rootPartition" == "0" ]] || {
        printf "%s\n" "${REVERSE}Please type which partition will be used as /boot.${NORMAL}" \
        "" \
        "That's necessary because you enabled both encryption and" \
        "multiboot options." \
        "${BOLD}" \
        "If you want to see which partitions are avaliable," \
        "please type ? and press enter." \
        "${NORMAL}" \

        read -p "Your choice (or partition): " -r bootPartition
        clear
        bootPartition="${bootPartition//\ /}"

        case "$bootPartition" in
            "?")
                NavigationTips
                fdisk -l "$storage" | less
                clear
                return 1
                ;;
            *)
                if [ -b "$bootPartition" ]
                then
                    IsDeviceOrPartition "$bootPartition" "true"
                    IsPartitionChild "$bootPartition" "$storage"
                    IsPartitionEqual "$rootPartition" "$bootPartition"
                else
                    InvalidStorageError
                fi
                ;;
        esac
    }
}

MultiBootEfi() {
    [ -d /sys/firmware/efi ] && {
        [ "$rootPartition" == "0" ] || {
        printf "%s\n" "${REVERSE}Please type which partition will be used as /efi.${NORMAL}" \
        "" \
        "The EFI Partition must contain a 'fat' filesystem." \
        "${BOLD}" \
        "If you want to see which partitions are avaliable," \
        "please type ? and press enter." \
        "${NORMAL}" \

        read -p "Your choice (or partition): " -r efiPartition
        clear
        efiPartition="${efiPartition//\ /}"

        case "$efiPartition" in
            "?")
                NavigationTips
                fdisk -l "$storage" | less
                clear
                return 1
                ;;
            *)
                if [ -b "$efiPartition" ]
                then
                    IsDeviceOrPartition "$efiPartition" "true"
                    IsPartitionChild "$efiPartition" "$storage"
                    IsPartitionEqual "$efiPartition" "$rootPartition"
                    IsPartitionEqual "$efiPartition" "$bootPartition"

                    df -T "$efiPartition" | grep -q "fat" || {
                        printf "%s\n\n" "${WARNING}The selected partition doesn't seem to be formatted as fat16 or fat32.${NORMAL}"
                        return 1
                    }
                else
                    InvalidStorageError
                fi
                ;;
        esac
    }
    }
}

Username() {
    printf "%s\n" "${REVERSE}What will your username be?${NORMAL}" \
    "${BOLD}" \
    "If you type nothing, the default 'poweruser' name will be set." \
    "All characters will be converted to lowercase." \
    "${NORMAL}"

    read -p "Your username: " -r username
    clear
    username="$(echo "$username" | sed -e 's/\ //g' -e 's/\(.*\)/\L\1/')"

    case "$username" in
        "")
            username="poweruser"
            ;;
        *)
            echo "$username" | grep -Eq "^[a-z_][a-z0-9_-]*[$]?$" || {
                InvalidGenericError
            }
            ;;
    esac
}

Password() {
    local password1
    local password2

    printf "%s\n" "${REVERSE}Please type the password for the user.${NORMAL}" \
    "${BOLD}" \
    "This password will also be used for root!" \
    "${NORMAL}"

    read -p "Your password: " -r -s password1
    printf "\n"

    read -p "Please type the password again: " -r -s password2
    clear

    if [ "$password1" == "$password2" ]
    then
        password="$password1"
    else
        PasswordMismatchError
    fi
}

SysHostname() {
    printf "%s\n" "${REVERSE}What will your hostname be?${NORMAL}" \
    "${BOLD}" \
    "If you type nothing, the default 'powerarch' name will be set." \
    "${NORMAL}"

    read -p "Your hostname: " -r hostname
    clear
    hostname="${hostname//\ /}"

    case "$hostname" in
        "")
            hostname="powerarch"
            ;;
        *)
            echo "$hostname" | grep -Eq "^[a-z_][a-z0-9_-]*[$]?$" || {
                InvalidGenericError
            }
            ;;
    esac
}

# Call each configuration step and repeat the current one if status=false
for Configuration in Preset Keyboard Locale Region Desktop Encryption Storage MultiBootRoot MultiBootBoot MultiBootEfi
#for Configuration in Keyboard Mirrors
do
    status=false
    while [ "$status" != true ]
    do
        if $Configuration
        then
            status=true
        fi
    done
done